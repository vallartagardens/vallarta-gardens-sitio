<? require('script/globals.php'); ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <? include_once($meta);?> 
    <link rel="icon" href="http://<?echo $GLOBALS['domain']?>/assets/dist/img/logos/icon.png">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
    <link href="../assets/dist/css/main.css" rel="stylesheet">
    <link href="../assets/dist/css/lightgallery.css" rel="stylesheet">
     <? include_once('script/js-main.php'); ?>
  </head>
  <body>
    <? include_once('modules/header.php');?>
    <? include_once('slider/main-slider.php');?>
    <? include_once('modules/nav.php');?>
    <div class="container vallarta-gardens">
      <div class="confirm">
        <hr class="featurette-divider">
         <h1 class="title">Thank you for reserving at</h1> <h1 class="title">Vallarta Gardens </h1>
         <p>We will contact you in order to complete your reservation</p>
         <hr>
         <p><a href="/"><button style="text-align: center;" type="button" class="btn btn-default btn-lg">BACK TO HOME</button></a></p>
         <hr class="featurette-divider">
      </div>
     

    </div><!-- /.container -->
    <div class="row accommodations">
      <? include_once('widgets/accommodations.php');?>  
    </div><!-- /.row -->
  <div class="social-media">
    <? include_once('modules/social_media.php');?>
  </div>
  <footer>
    <? include_once('modules/footer.php');?>
  </footer>
  </body>
 
</html>