<? require('script/globals.php'); ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <? include_once('meta-tags/events.php');?> 
    <link rel="icon" href="http://<?echo $GLOBALS['domain']?>/assets/dist/img/logos/icon.png">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
    <link href="../assets/dist/css/events.css" rel="stylesheet">
    <link href="../assets/dist/css/lightgallery.css" rel="stylesheet">
     <? include_once('script/js-main.php'); ?>
  </head>
  <body>
    <? include_once('modules/header.php');?>
    <? include_once('slider/events.php');?>
    <? include_once('modules/nav.php');?>
    <div class="container vallarta-gardens luxury-events">
      <h1 class="title">LUXURY EVENTS</h1>
           <div class="row">
        
         <p>Unique relationships deserve remarkable luxury weddings in spectacular surroundings. For a contemporary fairy tale marriage, let our expert wedding planners at Vallarta Gardens Resorts, take the weight off your shoulders so you can focus on the details of your bright future.</p>
         <hr class="featurette-divider2">
         <div class="portada img-full">
         <img  src="http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/events/luxury-weddings-vallarta-gardens-portada.jpg"/>
         </div>
         <hr class="featurette-divider2">
        <div class="lightgallery group-3">
          <a href="http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/events/luxury-weddings-vallarta-gardens-13.jpg">
          <img src="http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/events/luxury-weddings-vallarta-gardens-13.jpg"/>
          </a>
          <a href="http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/events/luxury-weddings-vallarta-gardens-2.jpg">
          <img src="http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/events/luxury-weddings-vallarta-gardens-2.jpg"/>
          </a>
          <a href="http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/events/luxury-weddings-vallarta-gardens-14.jpg">
          <img src="http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/events/luxury-weddings-vallarta-gardens-14.jpg"/>
          </a>
          <a href="http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/events/luxury-weddings-vallarta-gardens-6.jpg">
          <img src="http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/events/luxury-weddings-vallarta-gardens-6.jpg"/>
          </a>
          <a href="http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/events/luxury-weddings-vallarta-gardens-5.jpg">
          <img src="http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/events/luxury-weddings-vallarta-gardens-5.jpg"/>
          </a>
          <a href="http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/events/luxury-weddings-vallarta-gardens-16.jpg">
          <img src="http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/events/luxury-weddings-vallarta-gardens-16.jpg"/>
          </a>
        </div>
           <hr class="featurette-divider">
           <div class="row">
            <div class="col-6">
              <h1 style="font-size: 2em;">CONTACT US </h1>
               <? include_once('form/contact.php');?> 
            </div>
          <div class="col-6">
            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d955102.4139639356!2d-105.376407!3d20.756111!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x40f9357b39a0311d!2sVallarta+Gardens!5e0!3m2!1ses-419!2smx!4v1480313615576" class="google-map" frameborder="0" style="border:0" allowfullscreen></iframe> 
          </div>
           </div>

          
      </div>
    </div><!-- /.container -->
    <div class="row accommodations">
      <? include_once('widgets/accommodations.php');?>  
    </div><!-- /.row -->
  <div class="social-media">
    <? include_once('modules/social_media.php');?>
  </div>
  <footer>
    <? include_once('modules/footer.php');?>
  </footer>
  
  </body>
</html>
<script>
  $('.lightgallery').lightGallery();
</script>