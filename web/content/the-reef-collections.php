<div class="row featurette">
  <div class="col-md-7">
    <p class="lead intro">The Reef Collection is defined by the warmth and authenticity of its atmosphere, offering a distinctive ambiance in each unit, with unfailing attention to comfort and unique spaces for a memorable stay. Casa Vallarta, The Maine House and Martha’s Vineyard, are part of this collection, each provide a setting with singular style.</p>
  </div>
  <div class="col-md-5">    
    <div class="logo-accommodation" style="margin-top: 1em;">
    <img class="img-thumb" src="http://<?echo $GLOBALS['domain']?>/assets/dist/img/logos/Wid-Reef.jpg" alt="Generic placeholder image">
    </div>
  </div>
</div>
<!-- <hr class="featurette-divider"> -->
<br><br><br>
<div class="container">
  <ul class="nav nav-pills">
    <li class="active"><a data-toggle="pill" href="#home">GROUND FLOOR</a></li>
    <li><a data-toggle="pill" href="#menu1">MIDDLE FLOOR</a></li>
    <li><a data-toggle="pill" href="#menu2">PENTHOUSE</a></li>
        <li><a data-toggle="pill" href="#menu3">VIRTUAL TOUR</a></li>
  </ul>
  <div class="tab-content">
    <div id="home" class="tab-pane fade in active">
      <hr >
      <div class="row">  
        <div class="col-4">
          <img class="img-thumbnail" src="http://<?echo $GLOBALS['domain']?>/assets/dist/img/slider/villas/The-Reef-Collections/thumb-reef-collections-ground.jpg" alt="">
        </div><!-- /.col-lg-4 -->
        <div class="col-6 details-accommodations">
          <h3>The Reef Collections</h3>
          <h2>GROUND FLOOR</h2>
          <p><b>(2,066 square feet)</b></p>
          <p><b>Ocupancy:</b>4 adults and 2 kids (under 12 years old)</p>
          <hr class="featurette-divider2">
          <ul class="details">

            <li>Full kitchen</li>
            <li>Dining room</li>
            <li>Living room</li>
            <li>Entertainment room</li>
            <li>Master Bedroom(King size bed)</li>
            <li>Double Bedroom (Double size beds)</li>
            <li>2 Full Baths</li>
            <li>Terrace with private pool, grill, living and dining area Laundry room</li> 
          </ul>
          <ul class="booknow">
            <li class="gallery"><a href="#" id="ground-floor"><i  class="material-icons" title="VIEW GALLERY">&#xE439;</i><p>GALLERY</p></a></li>
            <li> <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">BOOK NOW</button></li>
          </ul>
          <hr class="featurette-divider2">
        </div><!-- /.col-lg-4 -->
        <div class="col-lg-2 floor-plan">
          <h2>Floor Plan</h2>
          <div class="lightgallery">
            <a href="http://<?echo $GLOBALS['domain']?>/assets/dist/img/floor-plans/floor-plans-the-reef-collection-ground-floor.jpg">
            <img class="img-circle" src="http://<?echo $GLOBALS['domain']?>/assets/dist/img/floor-plans/thumb-floor-plans-the-reef-collection-ground-floor.jpg" alt="">
            </a>
          </div>
        </div><!-- /.col-lg-4 -->
      </div><!-- /.row -->

    </div>
    <div id="menu1" class="tab-pane fade">
    <hr >
     <div class="row">  
        <div class="col-4">
          <img class="img-thumbnail" src="http://<?echo $GLOBALS['domain']?>/assets/dist/img/slider/villas/The-Reef-Collections/thumb-reef-collections-top.jpg" alt="">
        </div><!-- /.col-lg-4 -->
        <div class="col-lg-6 details-accommodations">
          <h3>The Reef Collections</h3>
          <h2>MIDDLE FLOOR</h2>
          <p><b>(2,823.80 square feet)</b></p>
          <p><b>Ocupancy:</b>4 adults and 2 kids (under 12 years old)</p>
          <hr class="featurette-divider2">
          <ul class="details">
            <li>Full kitchen</li>
            <li>Dining room</li>
            <li>Living room</li>
            <li>Entertainment room</li>
            <li>Master Bedroom(King size bed)</li>
            <li>Double Bedroom (Double size beds)</li>
            <li>2 Full Baths</li>
            <li>Terrace with Jacuzzi</li>
            <li>Laundry Room</li>
          </ul>
          <ul class="booknow">
            <li class="gallery"><a href="#" id="top-floor"><i  class="material-icons" title="VIEW GALLERY">&#xE439;</i><p>GALLERY</p></a></li>
            <li> <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">BOOK NOW</button></li>
          </ul>
          <hr class="featurette-divider2">   
        </div><!-- /.col-lg-4 -->
        <div class="col-2 floor-plan">
          <h2>Floor Plan</h2>
          <div class="lightgallery">
            <a href="http://<?echo $GLOBALS['domain']?>/assets/dist/img/floor-plans/floor-plans-the-reef-collection-top-floor.jpg">
            <img class="img-circle" src="http://<?echo $GLOBALS['domain']?>/assets/dist/img/floor-plans/thumb-floor-plans-the-reef-collection-top-floor.jpg" alt="">
            </a>
          </div>
        </div><!-- /.col-lg-4 -->
      </div><!-- /.row -->
    </div>
    <div id="menu2" class="tab-pane fade">
    <hr>
      <div class="row">  
        <div class="col-4">
          <img class="img-thumbnail" src="http://<?echo $GLOBALS['domain']?>/assets/dist/img/slider/villas/The-Reef-Collections/thumb-reef-collections-penthouse.jpg" alt="">
        </div><!-- /.col-lg-4 -->
        <div class="col-6 details-accommodations">
          <h3>The Reef Collections</h3>
          <h2>PENTHOUSE</h2>
          <p><b></b></p>
          <p><b>Ocupancy:</b>4 adults and 2 kids (under 12 years old)</p>
          <hr class="featurette-divider2">
          <div class="row">
            <div class="col-6">
              <h4 style="margin-left: 1em;">1ST. FLOOR</h4>
              <ul class="details">
                <li>Full kitchen</li>
                <li>Dining room</li>
                <li>Living room</li>
                <li>Entertainment room</li>
                <li>Master Bedroom(King size bed)</li>
                <li>Double Bedroom (Double size beds)</li>
                <li>2 Full Baths</li>
                <li>Laundry room</li> 
              </ul>
            </div>
            <div class="col-6">
              <h4 style="margin-left: 1em;">2ND. FLOOR</h4>
              <ul class="details">
                <li>Palapa terrace with ocean view</li>
                <li>Kitchenette (Fully equipped)</li>
                <li>Frigobar</li>
                <li>Grill</li>
                <li>Lounge space</li>
                <li>Living area with a 50" TV (Under palapa)</li>
                <li>Full bath</li>
                <li>Private pool</li>
              </ul>
            </div>
          </div>
         
          <ul class="booknow">
            <li class="gallery"><a href="#" id="penthouse"><i  class="material-icons" title="VIEW GALLERY">&#xE439;</i><p>GALLERY</p></a></li>
            <li> <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">BOOK NOW</button></li>
          </ul>
          <hr class="featurette-divider2">    
        </div><!-- /.col-lg-4 -->
        <div class="col-lg-2 floor-plan">
          <h2>Floor Plan</h2>
          <div class="lightgallery">
            <a href="http://<?echo $GLOBALS['domain']?>/assets/dist/img/floor-plans/floor-plans-the-reef-collection-penthouse-floor.jpg">
            <img class="img-circle" src="http://<?echo $GLOBALS['domain']?>/assets/dist/img/floor-plans/thumb-floor-plans-the-reef-collection-penthouse-floor.jpg" alt="">
            </a>
          </div>
        </div><!-- /.col-lg-4 -->
      </div><!-- /.row -->
    </div>
    <div id="menu3" class="tab-pane fade">
      <hr>
       <object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,0,0" width="1118" height="400" id="TourDelivery" align="middle"><param name="allowScriptAccess" value="always" /><param name="allowFullScreen" value="true" /><param name="WMode" value="opaque" /><param name="movie" value="http://fusion.realtourvision.com/widgetplayer.swf?version=4.3" /><param name="quality" value="high" /><param name="FlashVars" VALUE="tourid=541806" /><embed wmode="opaque" src="http://fusion.realtourvision.com/widgetplayer.swf?version=4.3" quality="high" flashvars="tourid=541806" width="1118" height="400" name="TourDelivery" align="middle" allowScriptAccess="always" allowFullScreen="true" type="application/x-shockwave-flash" pluginspage="http://www.adobe.com/go/getflashplayer" /></object>
    </div>
  </div>
</div>
<hr class="featurette-divider">
<div class="ammenities">
  <h3>Services and Amenities</h3>  
  <p><i>Restaurant / Spa / Gym / Private Beach / Water Sports / Swimming Pools / Jacuzzis / Tennis and / Basketball Courts / Concierge and Travel Agency / Daily Housekeeping / 24-Hour Security</i></p>
</div>

<script>
$('.lightgallery').lightGallery();

  $('#ground-floor').on('click', function() {
 
    $(this).lightGallery({
        dynamic: true,
        dynamicEl: [{
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/martas-vineyard-31a-1.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/martas-vineyard-31a-1.jpg'
            
        },
        {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/martas-vineyard-31a-2.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/martas-vineyard-31a-2.jpg'
            
        },
        {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/martas-vineyard-31a-3.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/martas-vineyard-31a-3.jpg'
            
        },
        {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/martas-vineyard-31a-4.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/martas-vineyard-31a-4.jpg'
            
        },
        {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/martas-vineyard-31a-5.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/martas-vineyard-31a-5.jpg'
            
        },
        {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/martas-vineyard-31a-6.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/martas-vineyard-31a-6.jpg'
            
        },
        {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/martas-vineyard-31a-7.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/martas-vineyard-31a-7.jpg'
            
        },

         {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/martas-vineyard-31a-8.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/martas-vineyard-31a-8.jpg'
            
        },
         {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/martas-vineyard-31a-9.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/martas-vineyard-31a-9.jpg'
            
        },
         {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/martas-vineyard-31a-10.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/martas-vineyard-31a-10.jpg'
            
        },
         {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/martas-vineyard-31a-11.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/martas-vineyard-31a-11.jpg'
            
        },
         {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/martas-vineyard-31a-12.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/martas-vineyard-31a-12.jpg'
            
        },
         {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/martas-vineyard-31a-13.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/martas-vineyard-31a-13.jpg'
            
        },
        {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/martas-vineyard-31a-14.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/martas-vineyard-31a-14.jpg'
            
        },
        {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/martas-vineyard-31a-15.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/martas-vineyard-31a-15.jpg'
            
        },
        {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/martas-vineyard-31a-16.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/martas-vineyard-31a-16.jpg'
            
        },
        {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/martas-vineyard-31a-17.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/martas-vineyard-31a-17.jpg'
            
        },
        {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/martas-vineyard-31a-18.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/martas-vineyard-31a-18.jpg'
            
        },
        {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/martas-vineyard-31a-19.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/martas-vineyard-31a-19.jpg'
            
        },
        {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/martas-vineyard-31a-20.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/martas-vineyard-31a-20.jpg'
            
        },
        {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/martas-vineyard-31a-21.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/martas-vineyard-31a-21.jpg'
            
        }]
    })
 
});

   $('#top-floor').on('click', function() {
 
    $(this).lightGallery({
        dynamic: true,
        dynamicEl: [{
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/top/marthas-vineyard-31b_00.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/top/marthas-vineyard-31b_00.jpg',
            
        },
        {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/top/marthas-vineyard-31b_1.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/top/marthas-vineyard-31b_1.jpg',
            
        },
        {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/top/marthas-vineyard-31b_2.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/top/marthas-vineyard-31b_2.jpg',
            
        },
        {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/top/marthas-vineyard-31b_3.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/top/marthas-vineyard-31b_3.jpg',
            
        },
        {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/top/marthas-vineyard-31b_4.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/top/marthas-vineyard-31b_4.jpg',
            
        },
        {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/top/marthas-vineyard-31b_5.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/top/marthas-vineyard-31b_5.jpg',
            
        },
        {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/top/marthas-vineyard-31b_6.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/top/marthas-vineyard-31b_6.jpg',
            
        },
        {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/top/marthas-vineyard-31b_7.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/top/marthas-vineyard-31b_7.jpg',
            
        },
        {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/top/marthas-vineyard-31b_8.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/top/marthas-vineyard-31b_8.jpg',
            
        },
        {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/top/marthas-vineyard-31b_9.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/top/marthas-vineyard-31b_9.jpg',
            
        },
        {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/top/marthas-vineyard-31b_10.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/top/marthas-vineyard-31b_10.jpg',
            
        }]
    })
 
});

   $('#penthouse').on('click', function() {
 
    $(this).lightGallery({
        dynamic: true,
        dynamicEl: [{
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/penthouse/martas-vineyard-31c-00.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/penthouse/martas-vineyard-31c-00.jpg',
            
        },
        {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/penthouse/martas-vineyard-31c-1.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/penthouse/martas-vineyard-31c-1.jpg',
            
        },
        {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/penthouse/martas-vineyard-31c-2.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/penthouse/martas-vineyard-31c-2.jpg',
            
        },
        {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/penthouse/martas-vineyard-31c-3.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/penthouse/martas-vineyard-31c-3.jpg',
            
        },
        {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/penthouse/martas-vineyard-31c-4.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/penthouse/martas-vineyard-31c-4.jpg',
            
        },
        {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/penthouse/martas-vineyard-31c-5.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/penthouse/martas-vineyard-31c-5.jpg',
            
        },
        {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/penthouse/martas-vineyard-31c-6.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/penthouse/martas-vineyard-31c-6.jpg',
            
        },
        {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/penthouse/martas-vineyard-31c-7.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/penthouse/martas-vineyard-31c-7.jpg',
            
        },
        {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/penthouse/martas-vineyard-31c-8.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/penthouse/martas-vineyard-31c-8.jpg',
            
        },
        {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/penthouse/martas-vineyard-31c-9.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/penthouse/martas-vineyard-31c-9.jpg',
            
        },
        {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/penthouse/martas-vineyard-31c-10.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/penthouse/martas-vineyard-31c-10.jpg',
            
        },
         {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/penthouse/martas-vineyard-31c-11.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/penthouse/martas-vineyard-31c-11.jpg',
            
        },
         {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/penthouse/martas-vineyard-31c-12.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/penthouse/martas-vineyard-31c-12.jpg',
            
        },
         {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/penthouse/martas-vineyard-31c-13.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/penthouse/martas-vineyard-31c-13.jpg',
            
        },
         {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/penthouse/martas-vineyard-31c-14.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/penthouse/martas-vineyard-31c-14.jpg',
            
        },
         {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/penthouse/martas-vineyard-31c-15.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/penthouse/martas-vineyard-31c-15.jpg',
            
        },
         {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/penthouse/martas-vineyard-31c-16.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/penthouse/martas-vineyard-31c-16.jpg',
            
        }]
    })
 
});
</script>