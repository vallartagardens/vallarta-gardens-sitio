<div class="row featurette">
  <div class="col-md-7">
    <p class="lead intro">Contemporary design and a majestic panoramic view of the bay make Casa Malabar one of a kind…
Entirely furnished and fully equipped, Casa Malabar embrace two and three bedroom units, wide open areas and
cozy private spaces; a splendid spot to sit back and admire the seascape.</p>
  </div>
  <div class="col-md-5">    
    <div class="logo-accommodation" style="margin-top: 1em;">
    <img class="img-thumb" src="http://<?echo $GLOBALS['domain']?>/assets/dist/img/logos/Logo-Malabar.png" alt="" width="50%">
    </div>
  </div>
</div>
<!-- <hr class="featurette-divider"> -->
<br><br><br>
<div class="container">
  <ul class="nav nav-pills">
    <li class="active"><a data-toggle="pill" href="#home">TWO BEDROOM</a></li>
    <li><a data-toggle="pill" href="#menu1">THREE BEDROOM</a></li>
  </ul>
  <div class="tab-content">
    <div id="home" class="tab-pane fade in active">
      <hr >
      <div class="row">  
        <div class="col-4">
          <img class="img-thumbnail" src="http://<?echo $GLOBALS['domain']?>/assets/dist/img/slider/villas/Casa-Malabar/two-bedroom.jpg" alt="">
        </div><!-- /.col-lg-4 -->
        <div class="col-6 details-accommodations">
          <h3>Casa Malabar</h3>
          <h2>TWO BEDROOM</h2>
          <p><b>(2,929.5 square feet)</b></p>
          <p><b>Ocupancy:</b>4 adults and 2 kids (under 12 years old)</p>
          <hr class="featurette-divider2">
          <ul class="details">
            <li>Full Kitchen</li>
            <li>Dining Room</li>
            <li>Living Room</li>
            <li>Master Bedroom (King size bed)</li>
            <li>Double Bedroom (Double size beds)</li>
            <li>2.5 baths</li>
            <li>Terrace with Private Pool, grill, living and dining area</li>
            <li>Laundry Room</li>
          </ul>
          <ul class="booknow">
            <li class="gallery"><a href="#" class="gallery-malabar"><i  class="material-icons" title="VIEW GALLERY">&#xE439;</i><p>GALLERY</p></a></li>
            <li> <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">BOOK NOW</button></li>
          </ul>
          <hr class="featurette-divider2">
        </div><!-- /.col-lg-4 -->
        <div class="col-2 floor-plan">
          <h2>Floor Plan</h2>
          <div class="lightgallery">
            <a href="http://<?echo $GLOBALS['domain']?>/assets/dist/img/floor-plans/floor-plans-casa-malabar.jpg">
            <img class="img-circle" src="http://<?echo $GLOBALS['domain']?>/assets/dist/img/floor-plans/thumb-floor-plans-casa-malabar.jpg" alt="">
            </a>
          </div>
        </div><!-- /.col-lg-4 -->
      </div><!-- /.row -->

    </div>
    <div id="menu1" class="tab-pane fade">
    <hr >
     <div class="row">  
        <div class="col-4">
          <img class="img-thumbnail" src="http://<?echo $GLOBALS['domain']?>/assets/dist/img/slider/villas/Casa-Malabar/three-bedroom.jpg" alt="">
        </div><!-- /.col-lg-4 -->
        <div class="col-6 details-accommodations">
          <h3>Casa Malabar</h3>
          <h2>THREE BEDROOM</h2>
          <p><b>(2,823.80 square feet)</b></p>
          <p><b>Ocupancy:</b>6 adults and 2 kids (under 12 years old)</p>
          <hr class="featurette-divider2">
          <ul class="details">
            <li>Full Kitchen</li>
            <li>Dining Room</li>
            <li>Living Room</li>
            <li>Master Bedroom (King size bed)</li>
            <li>Single Bedroom (King size bed)</li>
            <li>Double Bedroom (Double size beds)</li>
            <li>3Full Baths</li>
            <li>Terrace with Jacuzzi, grill, living and dining area</li>
            <li>Laundry Room</li>
          </ul>
          <ul class="booknow">
            <li class="gallery"><a href="#" class="gallery-malabar"><i  class="material-icons" title="VIEW GALLERY">&#xE439;</i><p>GALLERY</p></a></li>
            <li> <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">BOOK NOW</button></li>
          </ul>
          <hr class="featurette-divider2">   
        </div><!-- /.col-lg-4 -->
        <div class="col-2 floor-plan">
          <h2>Floor Plan</h2>
          <div class="lightgallery">
            <a href="http://<?echo $GLOBALS['domain']?>/assets/dist/img/floor-plans/floor-plans-casa-malabar-1.jpg">
            <img class="img-circle" src="http://<?echo $GLOBALS['domain']?>/assets/dist/img/floor-plans/thumb-floor-plans-casa-malabar-1.jpg" alt="">
            </a>
          </div>
        </div><!-- /.col-lg-4 -->
      </div><!-- /.row -->
    </div> 
  </div>
</div>
<hr class="featurette-divider">
<div class="ammenities">
  <h3>Services and Amenities</h3>  
  <p><i>Restaurant / Spa / Gym / Private Beach / Water Sports / Swimming Pools / Jacuzzis / Tennis and / Basketball Courts / Concierge and Travel Agency / Daily Housekeeping / 24-Hour Security</i></p>
</div>

<script>
$('.lightgallery').lightGallery();

  $('.gallery-malabar').on('click', function() {
 
    $(this).lightGallery({
        dynamic: true,
        dynamicEl: [{
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/malabar/casa-malabar-1.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/malabar/casa-malabar-1.jpg'
            
        },
        {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/malabar/casa-malabar-2.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/malabar/casa-malabar-2.jpg'
            
        },
        {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/malabar/casa-malabar-3.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/malabar/casa-malabar-3.jpg'
            
        },
        {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/malabar/casa-malabar-4.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/malabar/casa-malabar-4.jpg'
            
        },
        {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/malabar/casa-malabar-5.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/malabar/casa-malabar-5.jpg'
            
        },
        {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/malabar/casa-malabar-6.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/malabar/casa-malabar-6.jpg'
            
        },
        {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/malabar/casa-malabar-7.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/malabar/casa-malabar-7.jpg'
            
        },
        {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/malabar/casa-malabar-8.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/malabar/casa-malabar-8.jpg'
            
        },
        {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/malabar/casa-malabar-9.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/malabar/casa-malabar-9.jpg'
            
        },
        {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/malabar/casa-malabar-10.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/malabar/casa-malabar-10.jpg'
            
        },
        {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/malabar/casa-malabar-11.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/malabar/casa-malabar-11.jpg'
            
        },
        {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/malabar/casa-malabar-12.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/malabar/casa-malabar-12.jpg'
            
        },
        {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/malabar/casa-malabar-13.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/malabar/casa-malabar-13.jpg'
            
        },
        {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/malabar/casa-malabar-14.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/malabar/casa-malabar-14.jpg'
            
        },
        {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/malabar/casa-malabar-15.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/malabar/casa-malabar-15.jpg'
            
        },
        {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/malabar/casa-malabar-16.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/malabar/casa-malabar-16.jpg'
            
        },
        {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/malabar/casa-malabar-17.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/malabar/casa-malabar-17.jpg'
            
        },
        {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/malabar/casa-malabar-18.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/malabar/casa-malabar-18.jpg'
            
        },
        {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/malabar/casa-malabar-19.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/malabar/casa-malabar-19.jpg'
            
        }]
    })
 
});

   
</script>