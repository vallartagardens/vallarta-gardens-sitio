
<div class="row featurette">
  <div class="col-md-5">
    <h2 class="featurette-heading">TROPICAL PARADISE<p class="text-muted">The new hotspot in the Pacific Ocean</p></h2>
  </div>
  <div class="col-md-7">    
    <p class="lead intro">Vallarta Gardens, located in the heart of Riviera Nayarit in the quiet fishing village La Cruz De Huanacaxtle, provides the perfect setting for an unforgettable experien-ce. Only about 14 miles from Puerto Vallarta’s international airport (30 minutes) and 2 miles from the Marina Riviera Nayarit, the resort features gorgeous gardens and tranquil areas where the worries of everyday life just melt away. Vallarta Gardens offers the opportunity to vacation like never before. Its fully equi-pped units, decorated to thehighest standards of design, excellent service, unique culinary offerings and exclu-sive amenities promise an exceptional stay.</p>
  </div>
</div>

