
<div class="row featurette">
  <div class="col-md-7">
    <h2 class="featurette-heading">MEMBER SERVICES </h2>
    <p class="text-muted" style="font-size:1.2em">The Boutique Private Residence Club offers the latest in vacational programs; Flexibility, benefits and an array of services which makes us an excellent option for vacationing.</p>
  </div>
  <div class="col-md-5">    
  </div>
</div>
<hr class="featurette-divider">
<div class="row featurette">
  <div class="col-md-8">
    <div class="content-member">
      <p>These flexible services and benefits are designed to help you arrange and enjoy your vacation time each year. Be sure to sign up for our bimonthly bulletin, where you will find current information, updates, improvements and new projects, about which we eagerly await your comments.</p> 
      <p>Here, you can find general information about our services, developed to provide you the optimal experience from beginning to end.</p>
      <h3>Contact Information</h3>
      <hr>
      <ul>
        <li>Telephone</li>
        <li>Mexico: (329) 295-6212 / 6002 (country code 52)</li>
        <li>Toll-free USA: (855) 353-5859</li>
        <li>Toll-free Canada: (855) 267-8963</li>
      </ul>
      <h3>Department Emails</h3>
      <hr>
      <ul>
        <li>Reservations: <a href="mailto:reservations@boutiqueprc.com">reservations@boutiqueprc.com</a> </li>
        <li>Member Services:<a href="mailto:memberservices@boutiqueprc.com"> memberservices@boutiqueprc.com</a></li>
        <li>Financial: <a href="mailto:collections@boutiqueprc.com">collections@boutiqueprc.com</a> </li>
      </ul>
      <h3>To book your reservation, simply follow these steps:</h3>
      <hr>
      <ul>
          <li> Send us your <a href="/reservation/member">RESERVATION REQUEST</a> . (All reservations are subject to availability.)</li>
          <li> Pay fee. (If you are updating your week to a different season, be sure to pay the corresponding fee.)</li>
          <li> Within about 72 hours, the Reservations Department will be in touch to confirm your reservation.</li>
      </ul>
      <hr>
      <h4><b>Important Information</b></h4>
      <p>Be sure your payments are up to date before submitting your reservation request. Your fee payments must be current, including payments made through exchange companies. Proof of payment is needed to validate any change requested.
      To validate your reservation, a confirmation email from our Reservations Department in response to your request is needed.</p>

      <p>To cancel a reservation, you must notify us at least 45 days before your arrival date. If your cancellation notice is received less than 45 days before your arrival date, you will lose your week(s) without refund. (Provisions apply in cases of illness or the death of a direct family member.)</p>

      <p>To lend your week(s) to someone else, a written request with the name and number of guests is needed.</p>
      <p>You may request a larger unit than the one specified in your membership, in which case you will pay the difference between the corresponding fees. Keep in mind that all requests are subject to availability.</p>

      <p>Plan in advance! That way you can find more availability. In case you do not wish to request any weeks, remember that you can save those weeks.</p>
      <p>
        We remind you that membership weeks can not be divided into days.
        Platinum Memberships can be used Saturday to Saturday without date restrictions, except during vacation high seasons. You may also contact your exchange company.
        Regarding exchange companies, you can make the deposit for your weeks through us. Just call us, and we will be happy to assist you in submitting the deposit. To complete your deposit, you also need to pay the corresponding fee. You can also request your weeks directly with your exchange company.
      </p>
    </div>
  </div>
  <div class="col-4">
    <div class="container">
    <? include('plugins/members.php'); ?>
    </div>
  </div>
</div>
<style>
  .content-member p{
    font-size: 1.2em;
  }
</style>


<hr class="featurette-divider">
