<div class="row featurette">
  <div class="col-md-7">
    <p class="lead intro">With an impeccable distribution of spaces styled for comfort, relaxation and harmony, The Hamptons
offers a singular ambiance, private and quiet.The five level building provides a choice of entirely furnished and thoroughly equipped one bedroom units or studios.</p>
  </div>
  <div class="col-md-5">    
    <div class="logo-accommodation" style="margin-top: 1em;">
    <img class="img-thumb" src="http://<?echo $GLOBALS['domain']?>/assets/dist/img/logos/logo-The-Hamptons.jpg" alt="Generic placeholder image">
    </div>
  </div>
</div>
<!-- <hr class="featurette-divider"> -->
<br><br><br>
<div class="container">
  <ul class="nav nav-pills">
    <li class="active"><a data-toggle="pill" href="#home">ONE BEDROOM</a></li>
    <li><a data-toggle="pill" href="#menu1">STUDIO</a></li>
    <li><a data-toggle="pill" href="#menu2">BEDROOM LOCKOFF</a></li>
    <li><a data-toggle="pill" href="#menu3">PENTHOUSE</a></li>
    <li><a data-toggle="pill" href="#menu4">VIRTUAL TOUR</a></li>
  </ul>
  <div class="tab-content">
    <div id="home" class="tab-pane fade in active">
      <hr >
      <div class="row">  
        <div class="col-4">
          <img class="img-thumbnail" src="http://<?echo $GLOBALS['domain']?>/assets/dist/img/slider/villas/The-Hamptons/the-hamptons-ground-floor.jpg" alt="">
        </div><!-- /.col-lg-4 -->
        <div class="col-6 details-accommodations">
          <h3>The Hamptons</h3>
          <h2>ONE BEDROOM</h2>
          <p><b>(1,571 square feet)</b></p>
          <p><b>Ocupancy:</b>2 adults and 2 kids (under 12 years old)</p>
          <hr class="featurette-divider2">
          <ul class="details">
            <li>Full kitchen</li>
            <li>Dining room</li>
            <li>Living room</li>
            <li>Bedroom (King size bed)</li>
            <li>Full Bath</li>
            <li>Terrace</li>
            <br>
            <p><b>ADDITIONAL SERVICES:</b></p>
            <p>Laundry facilities and ice machine on site.</p>
          </ul>
          <ul class="booknow">
            <li class="gallery"><a href="#" class="hampton-gallery"><i  class="material-icons" title="VIEW GALLERY">&#xE439;</i><p>GALLERY</p></a></li>
            <li> <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">BOOK NOW</button></li>
          </ul>
          <hr class="featurette-divider2">
        </div><!-- /.col-lg-4 -->
        <div class="col-2 floor-plan">
          <h2>Floor Plan</h2>
          <div class="lightgallery">
            <a href="http://<?echo $GLOBALS['domain']?>/assets/dist/img/floor-plans/floor-plans-the-hamptons.jpg">
            <img class="img-circle" src="http://<?echo $GLOBALS['domain']?>/assets/dist/img/floor-plans/thumb-floor-plans-the-hamptons.jpg" alt="">
            </a>
          </div>
        </div><!-- /.col-lg-4 -->
      </div><!-- /.row -->

    </div>
    <div id="menu1" class="tab-pane fade">
    <hr >
     <div class="row">  
        <div class="col-4">
          <img class="img-thumbnail" src="http://<?echo $GLOBALS['domain']?>/assets/dist/img/slider/villas/The-Hamptons/the-hamptons-studio.jpg" alt="">
        </div><!-- /.col-lg-4 -->
        <div class="col-6 details-accommodations">
          <h3>The Hamptons</h3>
          <h2>STUDIO</h2>
          <p><b>(538 square feet)</b></p>
          <p><b>Ocupancy:</b>2 adults</p>
          <hr class="featurette-divider2">
          <ul class="details">
            <li>King size bed</li>
            <li>Full Bath</li>
            <li>Living area with a 60" TV</li>
            <li>Kitchenette</li>
            <li>Frigobar</li>
            <br>
            <p><b>ADDITIONAL SERVICES:</b></p>
            <p>Laundry facilities and ice machine on site.</p>
          </ul>
          <ul class="booknow">
            <li class="gallery"><a href="#" class="hampton-gallery"><i  class="material-icons" title="VIEW GALLERY">&#xE439;</i><p>GALLERY</p></a></li>
            <li> <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">BOOK NOW</button></li>
          </ul>
          <hr class="featurette-divider2">   
        </div><!-- /.col-lg-4 -->
        <div class="col-2 floor-plan">
          <h2>Floor Plan</h2>
          <div class="lightgallery">
            <a href="http://<?echo $GLOBALS['domain']?>/assets/dist/img/floor-plans/floor-plans-the-hamptons.jpg">
            <img class="img-circle" src="http://<?echo $GLOBALS['domain']?>/assets/dist/img/floor-plans/thumb-floor-plans-the-hamptons.jpg" alt="">
            </a>
          </div>
        </div><!-- /.col-lg-4 -->
      </div><!-- /.row -->
    </div>
    <div id="menu2" class="tab-pane fade">
    <hr>
      <div class="row">  
        <div class="col-4">
          <img class="img-thumbnail" src="http://<?echo $GLOBALS['domain']?>/assets/dist/img/slider/villas/The-Hamptons/the-hamptons-lockoff-floor.jpg" alt="">
        </div><!-- /.col-lg-4 -->
        <div class="col-6 details-accommodations">
          <h3>The Hamptons</h3>
          <h2>TWO BEDROOM LOCKOFF</h2>
          <p><b>(1,571 square feet)</b></p>
          <p><b>Ocupancy:</b> 4 adults and 2 kids (under 12 years old)</p>
          <hr class="featurette-divider2">
          <ul class="details">
            <li>One Bedroom Unit + Studio</li>
            
            <br>
            <p><b>ADDITIONAL SERVICES:</b></p>
            <p>Laundry facilities and ice machine on site.</p>
          </ul>
         
          <ul class="booknow">
            <li class="gallery"><a href="#" class="hampton-gallery"><i  class="material-icons" title="VIEW GALLERY">&#xE439;</i><p>GALLERY</p></a></li>
            <li> <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">BOOK NOW</button></li>
          </ul>
          <hr class="featurette-divider2">    
        </div><!-- /.col-lg-4 -->
        <div class="col-2 floor-plan">
          <h2>Floor Plan</h2>
          <div class="lightgallery">
            <a href="http://<?echo $GLOBALS['domain']?>/assets/dist/img/floor-plans/floor-plans-the-hamptons.jpg">
            <img class="img-circle" src="http://<?echo $GLOBALS['domain']?>/assets/dist/img/floor-plans/thumb-floor-plans-the-hamptons.jpg" alt="">
            </a>
          </div>
        </div><!-- /.col-lg-4 -->
      </div><!-- /.row -->
    </div>
    <div id="menu3" class="tab-pane fade">
     <hr>
      <div class="row">  
        <div class="col-4">
          <img class="img-thumbnail" src="http://<?echo $GLOBALS['domain']?>/assets/dist/img/slider/villas/The-Hamptons/the-hamptons-penthouse.jpg" alt="">
        </div><!-- /.col-lg-4 -->
        <div class="col-lg-6 details-accommodations">
          <h3>The Hamptons</h3>
          <h2>PENTHOUSE</h2>
          <p><b></b></p>
          <p><b>Ocupancy:</b>2 adults and 2 kids (under 12 years old)</p>
          <hr class="featurette-divider2">
          <div class="row">
            <div class="col-6">
              <h4 style="margin-left: 1em;">1ST. FLOOR</h4>
              <ul class="details">
                <li>Full kitchen</li>
                <li>Dining room</li>
                <li>Living room</li>
                <li>Bedroom (King size bed)</li>
                <li>1 Full Bath</li>
                <li>Terrace</li>
              </ul>
            </div>
            <div class="col-6">
              <h4 style="margin-left: 1em;">2ND. FLOOR</h4>
              <ul class="details">
                <li>Breathtaking covered terrace with</li>
                <li>amazing panoramic view of the bay</li>
                <li>Kitchenette (Fully equipped)</li>
                <li>Frigobar</li>
                <li>Grill</li>
                <li>Lounge space</li>
                <li>Living area with a 60"TV</li>
                <li>.5 Bath</li>
                <li>Jacuzzi</li>
              </ul>
            </div>
          </div>
         
          <ul class="booknow">
            <li class="gallery"><a href="#" class="hampton-gallery"><i  class="material-icons" title="VIEW GALLERY">&#xE439;</i><p>GALLERY</p></a></li>
            <li> <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">BOOK NOW</button></li>
          </ul>
          <hr class="featurette-divider2">    
        </div><!-- /.col-lg-4 -->
        <div class="col-2 floor-plan">
          <h2>Floor Plan</h2>
          <div class="lightgallery">
            <a href="http://<?echo $GLOBALS['domain']?>/assets/dist/img/floor-plans/floor-plans-the-hamptons.jpg">
            <img class="img-circle" src="http://<?echo $GLOBALS['domain']?>/assets/dist/img/floor-plans/thumb-floor-plans-the-hamptons.jpg" alt="">
            </a>
          </div>
        </div><!-- /.col-lg-4 -->
      </div><!-- /.row -->
      
    </div>
    <div id="menu4" class="tab-pane fade">
      <hr>
       
      
    </div>
  </div>
</div>
<hr class="featurette-divider">
<div class="ammenities">
  <h3>Services and Amenities</h3>  
  <p><i>Restaurant / Spa / Gym / Private Beach / Water Sports / Swimming Pools / Jacuzzis / Tennis and / Basketball Courts / Concierge and Travel Agency / Daily Housekeeping / 24-Hour Security</i></p>
</div>

<script>
$('.lightgallery').lightGallery();

  $('.hampton-gallery').on('click', function() {
 
    $(this).lightGallery({
        dynamic: true,
        dynamicEl: [{
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/hamptons/hampton-0.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/hamptons/hampton-0.jpg'
            
        },
        {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/hamptons/hampton-1.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/hamptons/hampton-1.jpg'
            
        },
        {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/hamptons/hampton-2.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/hamptons/hampton-2.jpg'
            
        },
        {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/hamptons/hampton-3.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/hamptons/hampton-3.jpg'
            
        },
        {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/hamptons/hampton-4.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/hamptons/hampton-4.jpg'
            
        },
        {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/hamptons/hampton-5.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/hamptons/hampton-5.jpg'
            
        },
        {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/hamptons/hampton-6.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/hamptons/hampton-6.jpg'
            
        },
        {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/hamptons/hampton-7.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/hamptons/hampton-7.jpg'
            
        },
        {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/hamptons/hampton-8.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/hamptons/hampton-8.jpg'
            
        },
        {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/hamptons/hampton-9.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/hamptons/hampton-9.jpg'
            
        },
        {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/hamptons/hampton-10.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/hamptons/hampton-10.jpg'
            
        },
        {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/hamptons/hampton-11.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/hamptons/hampton-11.jpg'
            
        },
        {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/hamptons/hampton-12.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/hamptons/hampton-12.jpg'
            
        },
        {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/hamptons/hampton-13.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/hamptons/hampton-13.jpg'
            
        },
        {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/hamptons/hampton-14.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/hamptons/hampton-14.jpg'
            
        },
        {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/hamptons/hampton-15.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/hamptons/hampton-15.jpg'
            
        },
        {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/hamptons/hampton-16.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/hamptons/hampton-16.jpg'
            
        },
        {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/hamptons/hampton-17.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/hamptons/hampton-17.jpg'
            
        },
        {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/hamptons/hampton-18.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/hamptons/hampton-18.jpg'
            
        },
        {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/hamptons/hampton-19.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/hamptons/hampton-19.jpg'
            
        },
        {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/hamptons/hampton-20.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/hamptons/hampton-20.jpg'
            
        },
        {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/hamptons/hampton-21.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/hamptons/hampton-21.jpg'
            
        },
        {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/hamptons/hampton-22.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/hamptons/hampton-22.jpg'
            
        }]
    })
 
});
</script>