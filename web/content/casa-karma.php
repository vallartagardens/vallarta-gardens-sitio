

<div class="row featurette">
  <div class="col-md-7">
    <p class="lead intro">Facing the bay and thus granting an unsurpassable view of the sea, the unique Casa Karma is fully furnished and appointed with exclusive decor. Lavishly equipped, it is the ideal haven for an ideal stay, providing wide comfortable spaces perfect for bringing together the entire family or a group of friends.</p>
  </div>
  <div class="col-md-5">    
    <div class="logo-accommodation" style="margin-top: 1em;">
    <img class="img-thumb" src="http://<?echo $GLOBALS['domain']?>/assets/dist/img/logos/logo-karma.png" alt="" width="50%">
    </div>
  </div>
</div>
<!-- <hr class="featurette-divider"> -->
<br><br><br>
<div class="container">
 
  <div class="tab-content">
    <div id="home" class="tab-pane fade in active">
      <hr >
      <div class="row">  
        <div class="col-4">
          <img class="img-thumbnail" src="http://<?echo $GLOBALS['domain']?>/assets/dist/img/slider/villas/Casa-Karma/casa-karma.jpg" alt="">
        </div><!-- /.col-lg-4 -->
        <div class="col-6 details-accommodations">
          <h3>Casa Karma</h3>
          <p><b>(8,374 square feet)</b></p>
          <p><b>Ocupancy:</b> 8 adults and 2 kids (under 12 years old)</p>
          <hr class="featurette-divider2">
          <ul class="details">
            <li>Full kitchen</li>
            <li>Dining room</li>
            <li>Living room</li>
            <li>Entertainment room</li>
            <li>Master bedroom with jacuzzi (King size bed)</li>
            <li>Single bedroom (King size bed)</li>
            <li>2 Double bedrooms (2 Double size beds each)</li>
            <li>5 Full Baths</li>
            <li>Terrace with private pool, grill, living and dining area</li>
            <li>Laundry room</li>
          </ul>

          <ul class="booknow">
            <li class="gallery"><a href="#" class="hampton-gallery"><i  class="material-icons" title="VIEW GALLERY">&#xE439;</i><p>GALLERY</p></a></li>
            
            <li> <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">BOOK NOW</button></li>
          </ul>

           

          <hr class="featurette-divider2">
        </div><!-- /.col-lg-4 -->
        <div class="col-2 floor-plan">
          <h2>Floor Plan</h2>
          <div class="lightgallery">
            <a href="http://<?echo $GLOBALS['domain']?>/assets/dist/img/floor-plans/casa-karma.jpg">
            <img class="img-circle" src="http://<?echo $GLOBALS['domain']?>/assets/dist/img/floor-plans/thumb-casa-karma.jpg" alt="">
            </a>
          </div>
        </div><!-- /.col-lg-4 -->
      </div><!-- /.row -->
    </div>
  </div>
</div>
<hr class="featurette-divider">
<div class="ammenities">
  <h3>Services and Amenities</h3>  
  <p><i>Restaurant / Spa / Gym / Private Beach / Water Sports / Swimming Pools / Jacuzzis / Tennis and / Basketball Courts / Concierge and Travel Agency / Daily Housekeeping / 24-Hour Security</i></p>
    
</div>

<script>

$('.date').daterangepicker();

$('.lightgallery').lightGallery();

  $('.hampton-gallery').on('click', function() {
 
    $(this).lightGallery({
        dynamic: true,
        dynamicEl: [{
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/karma/casa-karma-1.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/karma/casa-karma-1.jpg'
            
        },
        {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/karma/casa-karma-2.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/karma/casa-karma-2.jpg'
            
        },
        {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/karma/casa-karma-3.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/karma/casa-karma-3.jpg'
            
        },
        {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/karma/casa-karma-4.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/karma/casa-karma-4.jpg'
            
        },
        {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/karma/casa-karma-5.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/karma/casa-karma-5.jpg'
            
        },
        {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/karma/casa-karma-6.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/karma/casa-karma-6.jpg'
            
        },
        {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/karma/casa-karma-7.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/karma/casa-karma-7.jpg'
            
        },

         {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/karma/casa-karma-8.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/karma/casa-karma-8.jpg'
            
        },
         {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/karma/casa-karma-9.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/karma/casa-karma-9.jpg'
            
        },
         {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/karma/casa-karma-10.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/karma/casa-karma-10.jpg'
            
        },
         {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/karma/casa-karma-11.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/karma/casa-karma-11.jpg'
            
        },
         {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/karma/casa-karma-12.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/karma/casa-karma-12.jpg'
            
        },
         {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/karma/casa-karma-13.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/karma/casa-karma-13.jpg'
            
        },
        {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/karma/casa-karma-14.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/karma/casa-karma-14.jpg'
            
        },
        {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/karma/casa-karma-15.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/karma/casa-karma-15.jpg'
            
        },
        {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/karma/casa-karma-16.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/karma/casa-karma-16.jpg'
            
        },
        {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/karma/casa-karma-17.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/karma/casa-karma-17.jpg'
            
        },
        {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/karma/casa-karma-18.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/karma/casa-karma-18.jpg'
            
        },
        {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/karma/casa-karma-19.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/karma/casa-karma-19.jpg'
            
        },
        {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/karma/casa-karma-20.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/karma/casa-karma-20.jpg'
            
        },
        {
            "src": 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/karma/casa-karma-21.jpg',
            'thumb': 'http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/karma/casa-karma-21.jpg'
            
        }]
    })
 
});
</script>
