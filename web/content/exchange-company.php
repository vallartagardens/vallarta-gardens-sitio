
<div class="row featurette">
  <div class="col-md-7">
    <h2 class="featurette-heading">EXCHANGE COMPANY </h2>
    <p class="text-muted" style="font-size:1.2em">This exchange program is focused on resorts with the highest industry standards.</p>
  </div>
  <div class="col-md-5">    
  </div>
</div>
<hr class="featurette-divider">
<div class="row featurette">
  <div class="col-8">
    <div class="content-member">
      <h3>About The Registry Collection Program</h3>
      <p>The Registry Collection program is the world’s largest luxury exchange program, comprising more than 165 affiliates on six continents. Approximately 200 properties are available through The Registry Collection program and are either accessible for exchange or under development. From condo hotels and high-end fractional resorts to private residence clubs and fractional yachts, The Registry Collection program provides members with access to an elite network of the finest vacation properties at some of the world’s premier destinations, as well as personal concierge services that are available 24 hours a day. The Registry Collection program is offered by RCI, the worldwide leader in vacation exchange and part of the Wyndham Worldwide family of brands (NYSE: WYN). For additional information, visit our media center or <a href="https://www.theregistrycollection.com/" target="_blank">www.theregistrycollection.com.</a> </p>
      <hr>
      <h3>ULTIMATE LUXURY, UNLIMITED OPPORTUNITY</h3>
      <p>Welcome to the industry’s first and foremost luxury global exchange network product!</p>
      <p>As more and more baby boomers become affluent, the luxury leisure real estate market continues to grow. This discerning group of consumers wants and expects exclusivity and superlative quality not generally available at traditional shared-ownership properties.</p>
      <p>Celebrating 10 years of success, The Registry Collection® program aligns the interests of those attracted to high-end luxury leisure assets with the developers of extravagant properties to present the ultimate lifestyle purchase. We serve</p>
      
      <ul>
        <li>High-end Fractional Resorts</li>
        <li>Private Residence Clubs</li>
        <li>Select Whole Ownership Properties</li>
        <li>Fractional Yachts and Condo-Hotels.</li>
      </ul>
      <p>We view our 160 affiliates over six continents as our most important customers. Therefore, we are committed to delivering results-driven programs and services that provide an extremely valuable member proposition to your customers.</p>
      <h3>HOW DOES THE REGISTRY COLLECTION PROGRAM WORK?</h3>
      <hr>
      <p>While others are just entering this market, we are already enhancing our program. We facilitate activity and servicing among our nearly 35,000 members by providing three unique product components, all designed to offer a complete and exclusive luxury experience.</p>
      <p>The Registry Collection program offers more than 180 of the very finest destination properties, either accessible for exchange or under development, on six continents.</p>
      <p>The Registry Collection Partner Program offers owners incredible vacation experiences through our relationships with a host of leading providers, including.</p>
      <p>European Villas and Manors, a provider of European cottages, manor homes, villas, and more;Luxury Cruises, a provider of luxury cruise vacations, with access to thousands of itineraries and destinations;</p>
      <p>Off the Beaten Path, a provider of distinctive outdoor journeys, including fly-fishing expeditions, ranch vacations and much more;</p>
      <p>PerryGolf, a provider of customized luxury golf tours and cruises to 11 of the world’s most memorable destinations;
      </p>
      <p>Pure Luxury Holidays, a provider of the world’s finest and most exclusive five-star resorts, offering more than 240 hotels in 29 spectacular destinations worldwide;</p>
      <p>Yachtstore, a worldwide yacht charter service, specializing in luxury yacht charter vacations and sales; and Personal Concierge Services, a valuable lifestyle resource available to owners 24/7 from home or while on vacation.
      </p>
      <h3>Acknowledgment</h3>
      <p>It is the member’s responsibility to pay the annual affiliation fee and exchange fees that may be charged by the exchange companies. A Boutique Private Residence Beach Club shall pay in advance the first five (5) years of the member’s affiliation fees. Following this period, the Member has the option to renew, by paying the corresponding annual fee. A Boutique Private Residence Beach Club reserves sole discretion relating to future exchange availability. Exchange companies are independent, and all the fees relating to exchanges are the member’s responsibility.</p>
    </div>
  </div>
  <div class="col-4">
    <div class="container">
    <? include('plugins/members.php'); ?>
    </div>
  </div>
</div>
<style>
  .content-member p{
    font-size: 1.2em;
  }
</style>
<hr class="featurette-divider">
