
<div class="row featurette">
  <div class="col-md-7">
    <h2 class="featurette-heading">FINANCIAL DEPARTMENT </h2>
    <p class="text-muted" style="font-size:1.2em">We are committed to offering you the best service possible.</p>
  </div>
  <div class="col-md-5">    
  </div>
</div>
<hr class="featurette-divider">
<div class="row featurette">
  <div class="col-md-8">
    <div class="content-member">
      <h3>Financial / Collection policies.</h3>
      <p>Your stay depends on prompt payment of Residence Club fees. As stated in the Internal Rules agreement, it is your responsibility to cover said fees in the year that will be used. Only founders are exempt from this payment.</p>
      <p>Our Financial Department is at your full disposal, willing to assist you with every need, as it is not our wish to apply additional charges or penalties due to late payments, which may jeopardize your right of use or cause you to permanently lose your Membership.</p>
      <p>*As a reminder, you receive the statement for the current year each January, via email.</p>
      <h3>Debt Policies</h3>
      <hr>
      <ol>       
        <li>The maintenance fee  must be paid by February 15.</li>
        <li>The maintenance fee in effect on the date of the contract is stated on the cover page and will be increased or decreased annually to reflect US inflation as determined by reference to the national Consumer Price Index, or any future replacement thereof, changes in Hotel operating costs or changes in the wage level of hotel employees greater or less than the US inflation rate. Fees shall be paid in accordance with the instructions set forth in the internal policy.</li>
        <li>Members who have not submitted payment by February 15 will be notified by our Collections Department.</li>
        <li>There is a 10 percent penalty for late payments.</li>
        <li>There is no availability guarantee for members with late payments of any kind.</li>
        <li>Members who have not submitted payment by February 30 understand that their right of use will be denied, generating with this a reactivation fee of $150.00 USD.</li>
        <li>Every membership with payment pending on March 1 is automatically subject to a default interest fee of 10 percent annually.</li>
        <li> Members with payment pending on February 15 waive the right of use of their Membership until all payments have been submitted.</li>
      </ol>
      <h3>Financial Department</h3>
      <hr>
      <ul>
        <li>Telephone</li>
        <li>Mexico: (329) 295-6212 / 6002 (country code 52)</li>
        <li>Toll-free USA: (855) 353-5859</li>
        <li>Toll-free Canada: (855) 267-8963</li>
      </ul>
      <h3>Department Emails</h3>
      <hr>
      <ul>
        <li>Reservations:<a href="mailto:reservations@boutiqueprc.com">reservations@boutiqueprc.com</a> </li>
        <li>Member Services:<a href="mailto:memberservices@boutiqueprc.com">memberservices@boutiqueprc.com</a> </li>
        <li>Financial:<a href="mailto:collections@boutiqueprc.com">collections@boutiqueprc.com</a> </li>
      </ul>
      
    </div>
  </div>
  <div class="col-md-4">
    <div class="container">
    <? include('plugins/members.php'); ?>
    </div>
  </div>
</div>
<style>
  .content-member p{
    font-size: 1.2em;
  }
</style>


<hr class="featurette-divider">
