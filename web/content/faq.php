
<div class="row featurette">
  <div class="col-md-7">

    <h2 class="featurette-heading">Frequently Asked Questions</h2>
      <hr class="featurette-divider">
  </div>  
</div>

<div class="row featurette">
  <div class="col-md-8">
    <div class="content-member">
      
      <div id="accordion">
      <h1>How do I make my monthly payments?  Who do I contact?</h1>
      <div>Our Financial Department  is ready to assist and guide you regarding how to make all your payments. Toll-free USA: (855) 353-5859;  Toll-free Canada: (855) 267-8963;  Mexico: (329) 295-6212, ext. 421 (country code 52)</div>
      <h1>Can I pay with check or money order?</h1>
      <div>Yes, of course, you can.  All checks and money orders should be made payable to Deep Blue Desarrollos S de CV de RL and sent to A Boutique Private Residence, KM 1.2 Carr. Punta de Mita, La Cruz de Huanacaxtle, Bahía de Banderas, Nayarit, México 63734.</div>
      <h1>How can I request reservations?</h1>
      <div>You´re just one phone call away.  Dial ext. 204, and the Reservations Department  will be more than happy to assist you.  If you prefer, you can send us a Reservation Request,   and we will reply within the next 48 hours.
      Toll-free USA: (855) 353-5859,  Toll-free Canada: (855) 267-8963,  Mexico: (329) 295-6002, ext. 204 (country code 52)</div>
      <h1>How can I find out about my choices to go other places?</h1>
      <div>You can find out about a whole new world of options available through The Registry Collection.  Please visit their website and start planning your next vacation.</div>
      <h1>How can I use my Platinum Weeks?</h1>
      <div>These weeks are perfect for special occasions, when you are planning a family reunion or just want to celebrate with your loved ones.  They can be used in any type of unit, any time of the year (except holiday weeks). You just need to pay the corresponding HOAF.</div>
      <h1>How can I find the status of my account, the amount of my balance?</h1>
      <div>Contact our Finance Department for a detailed explanation of your account. Toll-free USA: (855) 353-5859;  Toll-free Canada: (855) 267-8963;  Mexico: (329) 295-6212, ext. 421 (country code 52); collections@boutiqueprc.com</div>
      <h1>How can I learn about my Membership?</h1>
      <div>Our Member Services Department will gladly explain your membership in detail.  Toll-free USA: (855) 353-5859;  Toll-free Canada: (855) 267-8963;  Mexico: (329) 295-6212, ext. 423 / 402 (country code 52);
      memberservices@boutiqueprc.com / memberservices1@boutiqueprc.com</div>
      <h1>Do you need something else?</h1>
      <div>Our Concierge will assist you in arranging everything to fulfill your vacation needs.  Just dial  ext. 414 from your Villa, and we´ll make all the arrangements for you to have the most amazing experience.</div>      
      </div>
      <hr>
      <div id="disqus_thread"></div>
    </div>
  </div>
  <div class="col-md-4">
    <div class="container">
    <? include('plugins/members.php'); ?>
    </div>
  </div>
</div>
<style>
  .content-member p{
    font-size: 1.2em;
  }
</style>
<script src="http://<?echo $GLOBALS['domain']?>/assets/dist/js/jquery-ui.js"></script>
<script>
$( "#accordion" ).accordion();
</script>

<hr class="featurette-divider">
<script>
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = '//vallarta-gardens.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})();
</script>