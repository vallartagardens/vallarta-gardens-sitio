<div class="overflow">
	<h1 class="subtitle">ATTENTION!</h1>
	<p><b>Your privacy and the security of communication between Vallarta Gardens; The Boutique Private Residence Club and its members are of paramount importance to us.</b></p>
	<p>	To ensure this privacy and security please be sure all contacts, communications and payments are handled exclusively through the channels listed below. We have learned from some of our members who had received calls or emails from other companies requesting for upfront payments related to your membership with us, for this reason, we would like to state Vallarta Gardens; The Boutique Private Residence Club is not related or linked to these companies in any way. </p>
	<p><b>If you receive any communications from any source other than one of these, or if you receive payment or other instructions that differ even in some small detail from those below, please report the contact immediately to our Member Services Department at the toll free number listed below.</b></p>
	<ul>
		<li>Toll Free USA. 855 353 5859 Ext. 423, 402</li>
		<li>Toll Free CAN.855 267 8963 Ext. 423, 402</li>
	</ul>
	<p>PAYMENTS: The only methods you should use for making payments to the Company are: </p>
	<ul>
	<li><b>FROST BANK -Check: </b>check payable to Vallarta Gardens Resorts LLC -Wire transfer: FROST BANK, SAN ANTONIO, TEXAS. Routing Number (ABA): 114 000 093 Swift Code: FRST US 44 Beneficiary Account Name: VALLARTA GARDENS RESORTS LLC Beneficiary Account Number: ACCOUNT # 010541818</li>
	<li><a href="#" target="_blank"><h5>PayPal</h5></a>
	<li><b>Credit Card:</b> Resort.com or Banamex (Punto54) (Through our Financial Department Executive)</li>
	<li>Payment inquires can be made using the telephone numbers noted below or by email: 
		Daniel Esperon (Vacation Club Manager): Ext. 421 <a href="mailto:danielesperon@boutiqueprc.com"> danielesperon@boutiqueprc.com</a></li>
	</ul>
	<p><b>Vallarta Gardens; The Boutique Private Residence Club: Wishes to take every precaution to protect our members from this ever happening. In the event that you are contacted by anyone asking you to make a payment, please verify by contacting your Member Services Representative or our Financial Department.</b></p>
</div>
<style>
	.overflow {
    background-color: #EFEEEE;
     height: 250px;
    overflow-x: hidden; /* Hide horizontal scrollbar */
    overflow-y: scroll;
    padding: 0 2em;
}
</style>