    
         
          <h2 class="featurette-heading">RESERVATION REQUEST FORM MEMBER</h2>
            <hr class="featurette-divider">
        
          <?php
        $action=$_REQUEST['action'];
        if ($action=="")  
            {
            ?>

            <form  action="" method="POST" enctype="multipart/form-data">
            <input type="hidden" name="action" value="submit">
            <div class="row">

            <div class="col-6">
            <label for="numberMembership" class="control-label" >Membership Number: *</label><br>
            <input name="numberMembership" type="text" value="" class="form-control" placeholder="fill only if you are member" /><br>

            <label for="sel1">Prefix: *</label>
              <select class="form-control" name="prefix" id="sel1" style="width: 30%;" >
                <option>Mr.</option>
                <option>Ms</option>
              </select>
            <label for="name" class="control-label">Name:</label><br>
            <input name="name" type="text" value="" class="form-control" placeholder="First Name" required/><br>
            <input name="lastname" type="text" value="" class="form-control" placeholder="Last Name" required/><br>

            <label for="email" class="control-label">Email:</label><br>
            <input name="email" type="mail" value="" class="form-control" placeholder="mail@domain.com" /><br>

             <label for="phone" class="control-label">Phone Number (Mobile) :</label><br>
            <input name="phone" type="text" value="" class="form-control" placeholder="Phone Number (Mobile)" /><br>

            <label for="daterange" class="control-label">Select dates: *</label><br>          
            <input type="text" class="date form-control" style="width: 40%;" name="daterange" value="" placeholder="Arrival Date - Departure Date" /><br>

            <label for="noAdults" class="control-label">No. of Adults: *</label>
            <input name="noAdults" type="text" value="" class="form-control" placeholder="" style="width: 40%;" required/>
            <label for="noChildren" class="control-label">No. of Children: *</label>
            <input name="noChildren" type="text" value="" class="form-control" style="width: 40%;"/>
            <label for="message" class="control-label">Comment:</label><br>
            <textarea name="message" rows="5" class="form-control"></textarea><br>
             <div class="container">
                <div class="overflow-information">
                <h2>IMPORTANT INFORMATION</h2>
                <ul>
                    <li>Your reservation will subject to availability.</li>
                    <li>Your reservation will be base in the type of unit that you purchased.</li>
                    <li>If you will like to get an upgrade its also subject to availability and an extra charge will be applied.</li>
                    <li>HOA fee must be covered in order to confirm your reservation</li>
                    <li>Once the reservation process is complete you will received a confirmation number.</li>
                    <li>Cancelation Policy: If cancelation request is received by e-mail at least 60 days prior to the arrival date HOA fee will be refunded in full, if less than 60 days no refund applies.  Please send your cancelation to:<a href="memberservices1@boutiqueprc.com">memberservices1@boutiqueprc.com</a>  and  <a href="miriamdeharo@boutiqueprc.com">miriamdeharo@boutiqueprc.com</a> </li>
                </ul>
           
                <h3>Groceries</h3>

                <p>This is the link to order your groceries online:  (All our members get 10% discount)
                <a href="http://www.shopnbag.delcampoasumesa.com/" target="_blank">Shop here</a></p>
                
                <ul>
                    <li>Your order must be processed 72 hours prior to your arrival or more.</li>
                    <li>Your groceries will be deliver to your villa prior to your arrival.</li>
                    <li>The cost of your groceries needs to be cover by you at the time your order your groceries with credit card.</li>
                </ul> 
                    
                </div>
            </div>
            </div>
            <div class="col-6">
                <h4>Select a Villa: *</h4>
                <div class="overflow">                                  
                <div class="row ">
                <div class="col-1">
                    <input name="casakarma" type="checkbox" value="Casa Karma" class="checkbox-inline" />
                </div>
                <div class="col-4">
                    <img src="http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/karma/casa-karma-11.jpg" style="width: 100%;">
                </div>
                <div class="col-7">
                  <h2>Casa Karma</h2>
                  <p><b>(8,374 square feet)</b></p>
                  <p><b>Ocupancy:</b> 8 adults and 2 kids (under 12 years old)</p>
                </div>
            </div>

             <div class="row">
                <hr>
                <div class="col-1">
                    <input name="casamalabarA" type="checkbox" value="Casa Malabar | Two bedroom" class="checkbox-inline" />
                </div>
                <div class="col-4">
                    <img src="http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/malabar/casa-malabar-1.jpg" style="width: 100%;">
                </div>
                <div class="col-7">
                  <h2>Casa Malabar | Two bedroom</h2>
                  <p><b>(2,929.5 square feet)</b></p>
                  <p><b>Ocupancy:</b>4 adults and 2 kids (under 12 years old)</p>
                </div>
            </div>
            <div class="row">
                <hr>
                <div class="col-1">
                    <input name="casamalabarB" type="checkbox" value="Casa Malabar | Three bedroom" class="checkbox-inline" />
                </div>
                <div class="col-4">
                    <img src="http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/malabar/casa-malabar-11.jpg" style="width: 100%;">
                </div>
                <div class="col-7">
                  <h2>Casa Malabar | Three bedroom</h2>
                  <p><b>(2,823.80 square feet)</b></p>
                  <p><b>Ocupancy:</b>6 adults and 2 kids (under 12 years old)</p>
                </div>
            </div>  
            <div class="row">
                <hr>
                <div class="col-1">
                    <input name="reefcollectionA" type="checkbox" value="The Reef Collections | Ground Floor" class="checkbox-inline" />
                </div>
                <div class="col-4">
                    <img src="http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/martas-vineyard-31a-1.jpg" style="width: 100%;">
                </div>
                <div class="col-7">
                  <h2>The Reef Collections | Ground Floor</h2>
                  <p><b>(2,066 square feet)</b></p>
                  <p><b>Ocupancy:</b>4 adults and 2 kids (under 12 years old)</p>
                </div>
            </div> 
            <div class="row">
                <hr>
                <div class="col-1">
                    <input name="reefcollectionB" type="checkbox" value="The Reef Collections | Middle Floor" class="checkbox-inline" />
                </div>
                <div class="col-4">
                    <img src="http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/top/marthas-vineyard-31b_00.jpg" style="width: 100%;">
                </div>
                <div class="col-7">
                  <h2>The Reef Collections | Middle Floor</h2>
                  <p><b>(2,823.80 square feet)</b></p>
                    <p><b>Ocupancy:</b>4 adults and 2 kids (under 12 years old)</p>
                </div>
            </div> 
            <div class="row">
                <hr>
                <div class="col-1">
                    <input name="reefcollectionC" type="checkbox" value="The Reef Collections | Penthouse" class="checkbox-inline" />
                </div>
                <div class="col-4">
                    <img src="http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/reef/penthouse/martas-vineyard-31c-00.jpg" style="width: 100%;">
                </div>
                <div class="col-7">
                   <h2>The Reef Collections | Penthouse</h2>
                  <p><b>Ocupancy:</b>4 adults and 2 kids (under 12 years old)</p>
                </div>
            </div> 
            <div class="row">
                <hr>
                <div class="col-1">
                    <input name="hamptonA" type="checkbox" value="The Hamptons | One bedroom" class="checkbox-inline" />
                </div>
                <div class="col-4">
                    <img src="http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/hamptons/hampton-9.jpg" style="width: 100%;">
                </div>
                <div class="col-7">
                  <h2>The Hamptons | One bedroom</h2>
                  <p><b>(1,571 square feet)</b></p>
                  <p><b>Ocupancy:</b>2 adults and 2 kids (under 12 years old)</p>
                </div>
            </div> 
            <div class="row">
                <hr>
                <div class="col-1">
                    <input name="hamptonB" type="checkbox" value="The Hamptons | Studio" class="checkbox-inline" />
                </div>
                <div class="col-4">
                    <img src="http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/hamptons/hampton-22.jpg" style="width: 100%;">
                </div>
                <div class="col-7">
                  <h2>The Hamptons | Studio</h2>
                  <p><b>(538 square feet)</b></p>
                  <p><b>Ocupancy:</b>2 adults</p>
                </div>
            </div> 
            <div class="row">
                <hr>
                <div class="col-1">
                    <input name="hamptonC" type="checkbox" value="The Hamptons | Two bedroom lockoff" class="checkbox-inline" />
                </div>
                <div class="col-4">
                    <img src="http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/hamptons/hampton-18.jpg" style="width: 100%;">
                </div>
                <div class="col-7">
                  <h2>The Hamptons | Two bedroom lockoff</h2>
                   <p><b>(1,571 square feet)</b></p>
                    <p><b>Ocupancy:</b> 4 adults and 2 kids (under 12 years old)</p>
                </div>
            </div> 
             <div class="row">
                <hr>
                <div class="col-1">
                    <input name="hamptonD" type="checkbox" value="The Hamptons | Penthouse" class="checkbox-inline" />
                </div>
                <div class="col-4">
                    <img src="http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/hamptons/hampton-14.jpg" style="width: 100%;">
                </div>
                <div class="col-7">
                  <h2>The Hamptons | Penthouse</h2>
                  <p><b>Ocupancy:</b>2 adults and 2 kids (under 12 years old)</p>
                </div>
            </div> 
            </div>
                </div>
            </div>     
            <input type="submit" class="btn btn-primary" value="Send reservations"/>
            </form>
            <?php
            } 
        else               
            {
            $member=$_REQUEST['member'];
            $numberMembership=$_REQUEST['numberMembership'];
            $prefix=$_REQUEST['prefix'];
            $message=$_REQUEST['message'];
            $name=$_REQUEST['name'];
            $lastname=$_REQUEST['lastname'];
            $email=$_REQUEST['email'];
            $phone=$_REQUEST['phone'];
            $daterange=$_REQUEST['daterange'];
            $noAdults=$_REQUEST['noAdults'];
            $noChildren=$_REQUEST['noChildren'];
            $casakarma=$_REQUEST['casakarma'];
            $casamalabarA=$_REQUEST['casamalabarA'];
            $casamalabarB=$_REQUEST['casamalabarB'];
            $reefcollectionA=$_REQUEST['reefcollectionA'];
            $reefcollectionB=$_REQUEST['reefcollectionB'];
            $reefcollectionC=$_REQUEST['reefcollectionC'];
            $hamptonA=$_REQUEST['hamptonA'];
            $hamptonB=$_REQUEST['hamptonB'];
            $hamptonC=$_REQUEST['hamptonC'];
            $hamptonD=$_REQUEST['hamptonD'];


            if (($name=="")||($email=="")||($message==""))
                {
                echo "All fields are required, please fill <a href=\"\">the form</a> again.";
                }
            else{    

                $to = 'info@boutiqueprc.com';
                $subject = 'Request Reservations from web Vallarta Gardens';               
                $headers = "From: " .  $email . "\r\n";
                $headers .= "Reply-To: ".$email . "\r\n";
                $headers .= "CC: webmaster@boutiqueprc.com,concierge@boutiqueprc.com,emarketing@boutiqueprc.com,reservations@boutiqueprc.com,comunicacion@boutiqueprc.com,mely20010@gmail.com\r\n";
                $headers .= "MIME-Version: 1.0\r\n";
                $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
                $message = '<html><body>';              
                $message .= "<h3 style='color:#505720; font-size:1.5em;'>Personal Data</h3>"."<h4>".$member." ".$numberMembership."</h4>"."<h4> Name:".$prefix." ".$name." ".$lastname."</h4>"."<h4> Email:".$email."</h4>"."<h4> Phone Number:".$phone."</h4>"."<hr>"."<h3 style='color:#505720; font-size:1.5em;'>Reservations</h3>"."<h4> Date range: ".$daterange."</h4>"."<h4> Number of Adults:".$noAdults."</h4>"."<h4> Number of Children:".$noChildren."</h4>"."<hr>"."<h3 style='color:#505720; font-size:1.5em;'> Accomodations:</h3>"."<h4>".$casakarma."</h4>"."<h4>".$casamalabarA."</h4>"."<h4>".$casamalabarB."</h4>"."<h4>".$reefcollectionA."</h4>"."<h4>".$reefcollectionB."</h4>"."<h4>".$reefcollectionC."</h4>". "<h4>".$hamptonA."</h4>"."<h4>".$hamptonB."</h4>". "<h4>".$hamptonC."</h4>". "<h4>".$hamptonD."</h4>";               
                $message .= "</body></html>";
                $message .= '<img src="http://cccspv.com/assets/dist/img/logos/logo-vallarta-gardens.png" alt="Logotipo"/>';                
                mail($to, $subject, $message, $headers);
                $url="/confirm"; 
                echo "<SCRIPT>window.location='$url';</SCRIPT>"; 

                }
                return true;
            }  
        ?> 
        </div>        
      </div>
    </div>
  </div>
</div>
<script>
    $('.date').daterangepicker();
</script>

 


