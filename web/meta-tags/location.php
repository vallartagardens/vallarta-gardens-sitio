<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="Luxury Villas, Casa Karma, Vallarta Gardens">
<meta name="author" content="Jazmin Aguirre Suárez">
<meta property="og:title" content="Vallarta Gardens, Casa Karma"/>
<meta property="og:image" content=".png"/>
<meta property="og:site_name" content="Jazmin Aguirre Suárez"/>
<meta property="og:description" content="Casa Karma Vallarta Gardens "graph" objects, allowing a certain level of customization over how information is carried over from a non-Facebook website to Facebook when a page is 'recommended', 'liked', or just generally shared."/>
<title>Location | Vallarta Gardens</title>   