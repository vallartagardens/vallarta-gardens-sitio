<div class="container">
<div class="l-footer">
  <div id="location" class="col-3">
    <h3>LOCATION</h3>
    <ul>
      <li>A BOUTIQUE PRIVATE RESIDENCE CLUB</li>
      <li>Km 1.2 Carretera federal</li>
      <li>La Cruz de Huanacaxtle,Punta de Mita </li>
      <li>Bahía de Banderas, Nay. | 63734.</li>
    </ul>
  </div>
  <div id="toll-free" class="col-3">
    <h3>TOLL FREE</h3>
    <ul>
      <li>USA:<a href="tel:8553535859"> 855 353 5859</a></li>
      <li>CAN:<a href="tel:8552678963"> 855 267 8963 </a></li>
      <li>MEX:<a href="tel:3292956212"> 329 295 6212</a> /<a href="tel:3292956002"> 6002</a></li>
      <li><a href="mailto:info@boutiqueprc.com">info@boutiqueprc.com</a></li>
    </ul>
  </div>
  <div id="accommodation" class="col-3">
    <h3>ACCOMMODATIONS</h3>
    <ul>
      <li><a href="/luxury-villas/casa-karma" title="Casa Karma">Casa Karma</a></li>
      <li><a href="/luxury-villas/casa-malabar" title="Casa Malabar">Casa Malabar</a></li>
      <li><a href="/luxury-villas/the-reef-collections" title="The Reef Collections">The Reef Collections</a></li>
      <li><a href="/luxury-villas/the-hamptons" title="The Hamptons">The Hamptons</a></li>
    </ul>
  </div>
  <div id="activities" class="col-3">
    <h3>PLACE AND THINGS TO DO</h3>
    <ul>
      <li><a href="#" title="Visit Riviera Nayarit">Riviera Nayarit</a></li>
      <li><a href="#" title="Puerto Vallarta">Puerto Vallarta</a></li>
      <li><a href="#" title="Calendar Activity">Calendar Activity</a></li>
      <li><a href="#" title="La Cruz de Huanacaxtle">La Cruz de Huanacaxtle</a></li>
    </ul>
  </div>
</div>
</div>