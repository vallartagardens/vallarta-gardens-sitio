<ul>
  <li><a href="https://www.facebook.com/VallartaGardensResort" target="_blank" title="Facebook"> <i class="socicon-facebook"></i></a></li>
  <li><a href="https://es.pinterest.com/aprivateboutiqu/resort/" target="_blank" title="Pinterest"><i class="socicon-pinterest"></i></a></li>
  <li><a href="https://twitter.com/vallartagarden" target="_blank" title="Twitter"><i class="socicon-twitter"></i></a></li>
  <li><a href="http://gramlove.co/user.php?id=2133722283&name=a_boutique_prc" target="_blank" title="Instagram"><i class="socicon-instagram"></i></a></li>
  <li><a href="https://plus.google.com/114433899719275697616" target="_blank" title="Google +"><i class="socicon-googleplus"></i></a></li>
  <li><a href="https://www.flickr.com/photos/aboutiqueprivateresidenceclub/albums" target="_blank" title="Flickr"><i class="socicon-flickr"></i></a></li>
  <li><a href="https://www.tripadvisor.com/UserReviewEdit-g1026781-d1570903-e__2F__Hotel__5F__Review__2D__g1026781__2D__d1570903__2D__Reviews__2D__Vallarta__5F__Gardens__5F__A__5F__Boutique__5F__Private__5F__Residence__5F__Club__2D__La__5F__Cruz__5F__de__5F__Huanacaxtle__5F__Pacific__5F__Coa__2E__html-Vallarta_Gardens_A_Boutique_Private_Residence_Club-La_Cruz_de_Huanacaxtle_Pacific_Coast.html" target="_blank"><i class="socicon-tripadvisor" title="tripadvisor"></i></a></li>
  <li><a href="mailto:info@boutiqueprc.com" target="_blank" title="info@boutiqueprc.com"> <i class="socicon-mail"></i></a></li>
  <li><a href="https://www.facebook.com/hashtag/vallartagardens?source=feed_text&story_id=1012314575560914" target="_blank" style="vertical-align: super;"> #vallartagardens </a></li>
</ul>