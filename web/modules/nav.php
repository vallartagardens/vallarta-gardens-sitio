
<nav class="navbar navbar-inverse navbar-static-top">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#"></a>
    </div>
    <div id="navbar" class="navbar-collapse collapse">
      <ul class="nav navbar-nav">
            <li class="dropdown active">
              <a href="/" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Home <span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="/destination">Destination</a></li>
                <li role="separator" class="divider"></li>
                <li><a href="/destination#rivieranayarit">Riviera Nayarit</a></li>
                <li><a href="/destination#puertovallarta">Puerto Vallarta</a></li>
                <li role="separator" class="divider"></li>
                <li><a href="/events">Events</a></li>
                <li><a href="/reviews">Reviews</a></li>  
                <li><a href="http://aboutiqueprivateresidenceclub.net/Principal-Blogs/index.php" target="_blank">Blogs</a></li>
              </ul>
            </li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Accommodations <span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="/luxury-villas/casa-karma">Casa Karma</a></li>
                <li role="separator" class="divider"></li>
                <li><a href="/luxury-villas/casa-malabar">Casa Malabar</a></li>
                <li role="separator" class="divider"></li>
                <li><a href="/luxury-villas/the-reef-collections">The Reef Collections</a></li>
                <li role="separator" class="divider"></li>
                <li><a href="/luxury-villas/the-hamptons">The Hamptons</a></li>
              </ul>
            </li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Member <span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="/members/member-services">Member Services </a></li>
                <li role="separator" class="divider"></li>
                <li><a href="/members/financial-department">Financial Deparment</a></li>
                <li role="separator" class="divider"></li>
                <li><a href="/members/exchange-company">Exchange Company</a></li>
                <li role="separator" class="divider"></li>
                <li><a href="/members/faq">Frequently Asked Questions</a></li>
                <li role="separator" class="divider"></li>
                <li><a href="http://www.aboutiqueprivateresidenceclub.mx/newsletteres_0607.php" target="_blank">Newsletter</a></li>
              </ul>
            </li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Reservations<span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="/reservation/member" >Member</a></li>
                <li role="separator" class="divider"></li>
                <li><a href="/reservation/guest">Guest</a></li>            
              </ul>
            </li>   
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Contact us<span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="/contact">Contact us</a></li>
                <li role="separator" class="divider"></li>
                <li><a href="/location">Location</a></li>           
              </ul>
            </li> 
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Language<span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="http://vallartagardens.com/">English</a></li>
                <li role="separator" class="divider"></li>
                <li><a href="http://aboutiqueprivateresidenceclub.com.mx/" target="_blank">Spanish</a></li>           
              </ul>
            </li>
          </ul>
        </li>
      </ul>
    </div>
  </div>
</nav>