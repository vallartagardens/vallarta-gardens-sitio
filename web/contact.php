<? require('script/globals.php'); ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <? include_once('meta-tags/contact.php');?> 
    <link rel="icon" href="http://<?echo $GLOBALS['domain']?>/assets/dist/img/logos/icon.png">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
    <link href="../assets/dist/css/main.css" rel="stylesheet">
    <link href="../assets/dist/css/lightgallery.css" rel="stylesheet">
     <? include_once('script/js-main.php'); ?>
  </head>
  <body>
    <? include_once('modules/header.php');?>
    <? include_once('slider/main-slider.php');?>
    <? include_once('modules/nav.php');?>
    <div class="container vallarta-gardens">
      <div class="row featurette">
  <div class="">
    <h2 class="featurette-heading">CONTACT US </h2>
    <p class="text-muted" style="font-size:1.2em"></p>
  </div>
  <hr class="featurette-divider">
<div class="row featurette">
  <div class="col-6">
      <div class="row">
      <? include_once('form/contact.php');?> 
  </div>
  </div>
  <div class="col-4">
 <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d955102.4139639356!2d-105.376407!3d20.756111!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x40f9357b39a0311d!2sVallarta+Gardens!5e0!3m2!1ses-419!2smx!4v1480313615576" width="380" height="500" frameborder="0" style="border:0" allowfullscreen></iframe> 
  </div>
</div>
</div>
    </div><!-- /.container -->
    <div class="row accommodations">
      <? include_once('widgets/accommodations.php');?>  
    </div><!-- /.row -->
  <div class="social-media">
    <? include_once('modules/social_media.php');?>
  </div>
  <footer>
    <? include_once('modules/footer.php');?>
  </footer>  
  </body>
</html>
<style>
  .overflow {
    background-color: #fff;
     height: 550px;
    overflow-x: hidden; /* Hide horizontal scrollbar */
    overflow-y: scroll;
}
.icon-comments{font-size: 2.5em;}
</style>