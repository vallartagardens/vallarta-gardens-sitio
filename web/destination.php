<? require('script/globals.php'); ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <? include_once('meta-tags/destination.php');?> 
    <link rel="icon" href="http://<?echo $GLOBALS['domain']?>/assets/dist/img/logos/icon.png">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
    <link href="../assets/dist/css/home.css" rel="stylesheet">
    <link href="../assets/dist/css/lightgallery.css" rel="stylesheet">
     <? include_once('script/js-main.php'); ?>
  </head>
  <body>
    <? include_once('modules/header.php');?>
    <? include_once('slider/main-slider.php');?>
    <? include_once('modules/nav.php');?>
    <div class="container vallarta-gardens">
      <div class="row featurette">
      <h2 class="featurette-heading">BANDERAS BAY </h2>
      <p class="text-muted" style="font-size:1.2em">Make plans early, to join in the fun!</p>
      <p class="text-muted" style="font-size:1.2em">Beautiful Banderas Bay or Bahia de Banderas is just one of the reasons why México is such a highly sought-after beach resort destination.</p>
      <hr class="featurette-divider">
      <p style="font-size: 1.3em;">The Pacific Ocean bay is Mexico’s largest, lapping the two Mexican states of Jalisco and Nayarit.
      
      Its long beautiful coastline runs for 42 miles. Banderas Bay is the number-one location for sports and eco adventures on the water, from parasailing and surfing to yachting from the port’s ritzy marina. Whale-watching in these waters is also popular, especially December to April when the whales come here to calve.
      
      Get out on the water of Banderas Bay in a sea-kayak, or cruise to one of the many islands dotting the bay.</p>
        <div class="lightgallery group-3">
          <a href="http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/destination/bahia-de-banderas-vallarta-gardens-1.jpg">
          <img src="http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/destination/bahia-de-banderas-vallarta-gardens-1.jpg"/>
          </a>
          <a href="http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/destination/bahia-de-banderas-vallarta-gardens-2.jpg">
          <img src="http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/destination/bahia-de-banderas-vallarta-gardens-2.jpg"/>
          </a>
          <a href="http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/destination/bahia-de-banderas-vallarta-gardens-3.jpg">
          <img src="http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/destination/bahia-de-banderas-vallarta-gardens-3.jpg"/>
          </a>
        </div>
        <label id="rivieranayarit"></label>
        <h2  class="featurette-heading">RIVIERA NAYARIT</h2>
        <p class="text-muted" style="font-size:1.2em">Riviera Nayarit is Mexico’s newest hot spot, a beach destination with 200 miles of pristine Pacific coastline</p>
        <hr>
        <p style="font-size: 1.3em;">Bejeweled with dozens of seaside towns and pueblos, it boasts lush mountain peaks, nature sanctuaries and exclusive resorts such as the Four Seasons and St. Regis. Affordable luxury, with six exceptional world-class golf courses designed by golf masters Jack Nicklaus, Greg Norman, Jim Lipe, Percy Clifford, Robert von Hagge, Michael Smelek and Rick Baril.

        Its most exciting addition is the world-class Marina Riviera Nayarit and Yacht Club, the marina offering nearly 400 slips accommodating boats up to 120 feet long.</p>
        <div class="lightgallery group-3">
          <a href="http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/destination/riviera-nayarit-puerto-vallarta-1.jpg">
          <img src="http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/destination/riviera-nayarit-puerto-vallarta-1.jpg"/>
          </a>
          <a href="http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/destination/riviera-nayarit-puerto-vallarta-2.jpg">
          <img src="http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/destination/riviera-nayarit-puerto-vallarta-2.jpg"/>
          </a>
          <a href="http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/destination/riviera-nayarit-puerto-vallarta-3.jpg">
          <img src="http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/destination/riviera-nayarit-puerto-vallarta-3.jpg"/>
          </a>
        </div>
        <label id="puertovallarta"></label>
        <h2 class="featurette-heading">PUERTO VALLARTA</h2>
        <p class="text-muted" style="font-size:1.2em">live it to believe it</p>
        <hr>
        <p style="font-size: 1.3em;">You never know when the gentle sounds of the ocean and the music of the bells will seem to be aligned for you at that most special moment. The fulfillment that comes from living in one of the most magical places in the world is what those living in Vallarta transmit to all visitors. A big smile radiating happiness is written on their faces.
        
        It is worth visiting Puerto Vallarta if only to feel and breathe in some of this joy.
        
        This magic captured from so many red sunsets has enticed nationals as well as foreigners from all over the world to make this magical bay their home. This chemistry imbues Puerto Vallarta with a creative, open and fulfilled multi-culture.</p>
        <div class="lightgallery group-3">
          <a href="http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/destination/vallarta-gardens-puerto-vallarta-1.jpg">
          <img src="http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/destination/vallarta-gardens-puerto-vallarta-1.jpg"/>
          </a>
          <a href="http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/destination/vallarta-gardens-puerto-vallarta-2.jpg">
          <img src="http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/destination/vallarta-gardens-puerto-vallarta-2.jpg"/>
          </a>
          <a href="http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/destination/vallarta-gardens-puerto-vallarta-3.jpg">
          <img src="http://<?echo $GLOBALS['domain']?>/assets/dist/img/gallery/destination/vallarta-gardens-puerto-vallarta-3.jpg"/>
          </a>
        </div>
      </div>
    </div><!-- /.container -->
    <div class="row accommodations">
      <? include_once('widgets/accommodations.php');?>  
    </div><!-- /.row -->
  <div class="social-media">
    <? include_once('modules/social_media.php');?>
  </div>
  <footer>
    <? include_once('modules/footer.php');?>
  </footer>
  
  </body>
</html>
<script>
  $('.lightgallery').lightGallery();
</script>
