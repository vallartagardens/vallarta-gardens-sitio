<? require('script/globals.php'); ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <? include_once('meta-tags/reviews.php');?> 
    <link rel="icon" href="http://<?echo $GLOBALS['domain']?>/assets/dist/img/logos/icon.png">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
    <link href="../assets/dist/css/main.css" rel="stylesheet">
    <link href="../assets/dist/css/lightgallery.css" rel="stylesheet">
     <? include_once('script/js-main.php'); ?>
  </head>
  <body>
    <? include_once('modules/header.php');?>
    <? include_once('slider/main-slider.php');?>
    <? include_once('modules/nav.php');?>
    <div class="container vallarta-gardens">
      <div class="row featurette">
  <div class="col-md-7">
    <h2 class="featurette-heading">REVIEWS </h2>
    <p class="text-muted" style="font-size:1.2em"></p>
  </div>
  <div class="col-md-5">    
  </div>
</div>
<hr class="featurette-divider">
<div class="row featurette">
  <div class="col-md-8">
    <div class="overflow">    
      <p><i class="material-icons icon-comments">&#xE0B9;</i>
      We wanted to let you know how much we appreciated all you did for our daughter, son-in-law and their friends during their recent stay at Vallarta Gardens. They had an incredible time and could not stop talking about the kind staff, wonderful accommodations and amazing resort. We are so pleased that they had such an amazing time and are so proud of our new ownership.
      
      The resort went above and beyond to make their stay memorable and we can’t thank you enough. Again, we can’t wait to beautiful Vallarta Gardens.
      
      Thank you so much!!!!</p>
      <p> <i><b>Roger and Shari A.(June 2015)</b></i></p>
      <hr>
      <p><i class="material-icons icon-comments">&#xE0B9;</i>
      Beautiful property and wonderful people!!
      </p><p>
      <i><b>Bob K.(June 2015)</b></i></p>
      <hr>
      <p><i class="material-icons icon-comments">&#xE0B9;</i>
      Beautiful location and premises.
      </p><p><i><b>Sayda L.(May 2015)</b></i></p>
      <hr>
      <p><i class="material-icons icon-comments">&#xE0B9;</i>
      Vallarta Gardens is absolutely beautiful. Each roomis charming and unique. We stayed in Casa Karma, which had a breathtaking view and was perfect for a group. We held our wedding with the help of the resort’s wedding planner, everyone says it was the best wedding they had ever been to. The wedding planner was amazing, she was responsive, easy to work with and made huge efforts to make our wedding perfect. She double checked everything, was on time and followed the schedule. The wedding was at the private beach. We all loved the delicious food from the restaurant. I highly recommend Vallarta Gardens. 
      </p><p><i><b>Leonard Peyton(April 2015)</b></i></p>
      <hr>
      <p><i class="material-icons icon-comments">&#xE0B9;</i>
      The hospitality staff is excellent.
      </p><p>
      <i><b>Jack B.(April 2015)</b></i></p>
      <hr>
      <p><i class="material-icons icon-comments">&#xE0B9;</i>
      The trip was amazing!! We had a great time in this wonderful villa and the staff was more than attentive. All of our children were looked after well and made to feel very safe. The house staff kept the house super tidy and were friendly. The restaurant and bar staff were always ready for us and were in such a good mood everyday!! We loved it and I would totally recommend this resort and the home to all my friends. Thank you so much for everything and for making this one of our best trips ever!
      </p><p>
      <i><b>Daiana (Abril 2015)</b></i></p>
      <hr>
      <p><i class="material-icons icon-comments">&#xE0B9;</i>
      Beautiful and very clean.
      </p>
      <p><i><b>Trellys H.(March 2015)</b></i></p>
      <hr>
      <p><i class="material-icons icon-comments">&#xE0B9;</i>
      This is our second evening and everything about Vallarta Gardens has exceeded our expectations. We’ve kayaked through the waves, had a couple strolls through town, and best of all just relaxed. It is very warm now, but we are lucky enough to have a private pool, which is extremely refreshing. Our villa is beautiful. We have an incredible view of the bay from both the bedroom and our living room.
      </p>
      <p><i><b>Thomas Brown (May 2015)</b></i></p>
      <hr>
      <p><i class="material-icons icon-comments">&#xE0B9;</i>
      We had a very interesting experience at Vallarta Gardens. Upon arriving to the complex, we discovered that the resort is most definitely not in Puerto Vallarta, but is actually out in the middle of nowhere. The surrounding area did not seem like the safest place to be and there were no restaurants within walking distance. So, if you think that you will be going to the heart of Puerto Vallarta, you will not. It is a 15-20 minutes cab ride into the city.
      </p>
      <p><i><b>Paul King (October 2015)</b></i></p>
      <hr>
     <p><i class="material-icons icon-comments">&#xE0B9;</i>
      We went there last summer, me and 4 friends, we stayed in a 4 room villa, we planned our vacations ahead of time. A friend of ours had been there before and highly recommended the place. We stayed there for 5 days, and had a blast. We went kayaking; the resort provided the kayaks of course. We also went fishing (it was my first time, but among my friends one has been fisherman for a while). The next day we went to one of the golf courses we were recommended and loved the resort more when we were told, we had a discount for being guests at Vallarta Gardens.
      </p>
      <p><i><b>Jhon Vance (October 2015)</b></i></p>
      <hr>
      
    </div>

    <div id="disqus_thread"></div>
  </div>
  </div>
  <div class="col-md-4">
  
  </div>
</div>

      
<script>


(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = '//vallarta-gardens.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})();
</script>
                                

    </div><!-- /.container -->
    <div class="row accommodations">
      <? include_once('widgets/accommodations.php');?>  
    </div><!-- /.row -->
  <div class="social-media">
    <? include_once('modules/social_media.php');?>
  </div>
  <footer>
    <? include_once('modules/footer.php');?>
  </footer>
  
  </body>
</html>
<style>
  .overflow {
    background-color: #fff;
     height: 550px;
    overflow-x: hidden; /* Hide horizontal scrollbar */
    overflow-y: scroll;
}
.icon-comments{font-size: 2.5em;}
</style>