<div id="myCarousel" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="1"></li>
    <li data-target="#myCarousel" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner" role="listbox">
    <div class="item active">
      <img class="first-slide" src="http://<?echo $GLOBALS['domain']?>/assets/dist/img/slider/villas/The-Reef-Collections/hoteles-en-riviera-nayarit-vallarta-gardens-the-reef-collections-1.jpg" alt="First slide">
      <div class="container">
        <div class="carousel-caption">         
        </div>
      </div>
    </div>
    <div class="item">
      <img class="second-slide" src="http://<?echo $GLOBALS['domain']?>/assets/dist/img/slider/villas/The-Reef-Collections/hoteles-en-riviera-nayarit-vallarta-gardens-the-reef-collections-2.jpg" alt="Second slide">
      <div class="container">
        <div class="carousel-caption">        
        </div>
      </div>
    </div>
    <div class="item">
      <img class="third-slide" src="http://<?echo $GLOBALS['domain']?>/assets/dist/img/slider/villas/The-Reef-Collections/hoteles-en-riviera-nayarit-vallarta-gardens-the-reef-collections-3.jpg" alt="Third slide">
      <div class="container">
        <div class="carousel-caption">
        </div>
      </div>
    </div>
    <div class="item">
      <img class="third-slide" src="http://<?echo $GLOBALS['domain']?>/assets/dist/img/slider/villas/The-Reef-Collections/hoteles-en-riviera-nayarit-vallarta-gardens-the-reef-collections-4.jpg" alt="Third slide">
      <div class="container">
        <div class="carousel-caption">
        </div>
      </div>
    </div>
  </div>
  <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
    <span class="sr-only">Next</span>
  </a>
</div>
