<? require('script/globals.php'); ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <? include_once($meta);?> 
    <link rel="icon" href="http://<?echo $GLOBALS['domain']?>/assets/dist/img/logos/icon.png">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
    <link href="../assets/dist/css/accommodation.css" rel="stylesheet">
    <link href="../assets/dist/css/lightgallery.css" rel="stylesheet">
     <? include_once('script/js-main.php'); ?>
  </head>
  <body>
    <? include_once('modules/header.php');?>
    <? //include_once('slider/main-slider.php');?>
    <? include_once($slider);?>
    <? include_once('modules/nav.php');?>
    <div class="container vallarta-gardens">
      <? include_once($content);?>   
    </div><!-- /.container -->
    <div class="row accommodations">
      <? include_once('widgets/accommodations.php');?>  
       <? include_once('form/booking.php'); ?> 
    </div><!-- /.row -->
  <div class="social-media">
    <? include_once('modules/social_media.php');?>
  </div>
  <footer>
    <? include_once('modules/footer.php');?>
  </footer>
  
  </body>
</html>