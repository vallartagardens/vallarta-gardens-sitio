<? require('script/globals.php'); ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="http://<?echo $GLOBALS['domain']?>/assets/dist/img/logos/icon.png">
    <title>Vallarta Gardens</title>
    <link href="../assets/dist/css/home.css" rel="stylesheet">
  </head>
  <body>
    <? include_once('modules/header.php');?>
    <? include_once('slider/main-slider.php');?>
    <? include_once('modules/nav.php');?>
    <div class="container vallarta-gardens">
      <? include_once('content/main.php');?>   
    </div><!-- /.container -->
    <div class="row accommodations">
      <? include_once('widgets/accommodations.php');?>  
    </div><!-- /.accommodation -->
  <div class="social-media">
    <? include_once('modules/social_media.php');?>
  </div>
  <footer>
    <? include_once('modules/footer.php');?>
  </footer>
   <? include_once('script/js-main.php'); ?>
  </body>
</html>