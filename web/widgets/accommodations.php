<hr class="featurette-divider">
<div class="row">
<div class="container villas-footer">
  <h1 style="text-align: center;">OUR VILLAS</h1>
  <div class="col-lg-3">
    <img class="img-thumb" src="http://<?echo $GLOBALS['domain']?>/assets/dist/img/lists/list-accommodation/karma.jpg" alt="Generic placeholder image" width="100%">
    <h2 class="title-accommodation"><a href="/luxury-villas/casa-karma">Casa Karma</a></h2>
    <p>Facing the bay and thus granting an unsurpassable view of the sea, the unique Casa Karma...</p>
    <p><a class="btn btn-primary" href="/luxury-villas/casa-karma" role="button">View details &raquo;</a></p>
  </div><!-- /.col-lg-4 -->
  <div class="col-lg-3">
    <img class="img-thumb" src="http://<?echo $GLOBALS['domain']?>/assets/dist/img/lists/list-accommodation/malabar.jpg" alt="Generic placeholder image" width="100%">
    <h2 class="title-accommodation"><a href="/luxury-villas/casa-malabar"> Casa Malabar</a></h2>
    <p>Contemporary design and a majestic panoramic view of the bay make Casa Malabar one of a kind…</p>
    <p><a class="btn btn-primary" href="/luxury-villas/casa-malabar" role="button">View details &raquo;</a></p>
  </div><!-- /.col-lg-4 -->
  <div class="col-lg-3">
    <img class="img-thumb" src="http://<?echo $GLOBALS['domain']?>/assets/dist/img/lists/list-accommodation/reef.jpg" alt="Generic placeholder image" width="100%">
    <h2 class="title-accommodation"><a href="/luxury-villas/the-reef-collections">The Reef Collections</a></h2>
    <p>The Reef Collection is defined by the warmth and authenticity of its atmosphere, offering a distinctive...</p>
    <p><a class="btn btn-primary" href="/luxury-villas/the-reef-collections" role="button">View details &raquo;</a></p>
  </div><!-- /.col-lg-4 -->
  <div class="col-lg-3">
    <img class="img-thumb" src="http://<?echo $GLOBALS['domain']?>/assets/dist/img/lists/list-accommodation/hampton.jpg" alt="Generic placeholder image" width="100%">
    <h2 class="title-accommodation"><a href="/luxury-villas/the-hamptons">The Hamptons</a></h2>
    <p>With an impeccable distribution of spaces styled for comfort, relaxation and harmony, The Hamptons...</p>
    <p><a class="btn btn-primary" href="/luxury-villas/the-hamptons" role="button">View details &raquo;</a></p>
  </div><!-- /.col-lg-4 -->
  </div>
</div><!-- /.row -->
 <hr class="featurette-divider">
 <style>
   .title-accommodation{
    font-size: 1.9em;
   }
   .villas-footer p{

    font-size: 1.2em;

   }
 </style>